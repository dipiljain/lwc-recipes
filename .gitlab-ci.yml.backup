
image: salesforce/salesforcedx:latest

stages:
  - execute_unit_tests
  - code_quality_scan
  - unlocked_package
  - QA_install
  - SIT_install
  - UAT_install
  - PROD_install

variables:
    SFDX_TIMEOUT: "240"
    SRC_PATH: "force-app"

####################################################
# Runs LWC tests on the runner. No org needed.
# This is a good predeploy stage to avoid creating
# a scratch org if tests are already failing.
# Set `allow_failure: true` to not hold up the
# pipeline on failure.
####################################################

code-quality-scan:
  stage: code_quality_scan
  allow_failure: true
  script:
    - sfdx force:lightning:lint force-app/main/default/aura --targetusername $CI_JOB_ID >> LightningReport.txt 2>&1
 artifacts:
    paths:
    - LightningReport.txt
    expire_in: 7 days

run-unit-tests:
    stage: execute_unit_tests
    script:
      - sfdx --version
      - echo "$GITLAB_USER_NAME triggered run-unit-tests on $(date)";
      - echo -e $DEVHUB_AUTH_URL >> devhub-auth.txt
      - sfdx force:auth:sfdxurl:store --sfdxurlfile devhub-auth.txt --setalias $DEVHUB_ORG --setdefaultdevhubusername
      - sfdx force:org:create --setdefaultusername -f config/project-scratch-def.json --setalias $CI_JOB_ID
      - sfdx force:user:display -u $CI_JOB_ID > UserDetails.txt
      - sfdx force:source:deploy -p force-app -w 10
      - sfdx force:apex:test:run --verbose -c -r human -d testLog -w 10
    when: always
    allow_failure: true
    artifacts:
    paths:
      - UserDetails.txt
      - testLog
    expire_in: 7 days

create-package:
  stage: unlocked_package
    script:
      - sfdx --version
      - echo "$GITLAB_USER_NAME triggered create-package on $(date)";
      - echo -e $DEVHUB_AUTH_URL >> devhub-auth.txt
      - sfdx force:auth:sfdxurl:store --sfdxurlfile devhub-auth.txt --setalias $DEVHUB_ORG --setdefaultdevhubusername
      #- sfdx force:package:version:create -p $PACKAGE_NAME -w 10 --description "($PACKAGE_NAME) created by CI/CD" --installationkeybypass --skipvalidation -f config/project-scratch-def.json
      cmd="sfdx force:package:version:create --nonamespace --targetdevhubusername  $devhub_username --package $package_id --versionnumber $version_number --installationkeybypass --wait 10 --json" && (echo $cmd >&2)
    output=$($cmd) && (echo $output | jq '.' >&2)
    local subscriber_package_version_id=$(jq -r '.result.SubscriberPackageVersionId' <<< $output)

    if [[ -z "$subscriber_package_version_id" || $subscriber_package_version_id == null ]]; then
      echo "ERROR No subscriber package version found for package id '$package_id'" >&2
      exit 1
    fi
    only:
      - main
    when: on_success
    allow_failure: false

qa-org-install
  stage: QA_install
  script:
    - sfdx --version
    - echo "$GITLAB_USER_NAME triggered create-package on $(date)";
    - echo -e $DEVHUB_AUTH_URL >> devhub-auth.txt
    - sfdx force:auth:sfdxurl:store --sfdxurlfile devhub-auth.txt --setalias $DEVHUB_ORG --setdefaultdevhubusername
    - sfdx forc:package:version:install -p 
      


# Function to authenticate to Salesforce.
  # Don't expose the auth url to the logs.
  # Arguments:
  #     $1 = alias to set
  #     $2 = Sfdx Auth URL
  #     $3 = SFDX AUth URL to use if the previous one isn't set (optional)

  function authenticate() {

    local alias_to_set=$1
    local org_auth_url=$2
    local org_auth_url_backup=$3

    local file=$(mktemp)
    echo $org_auth_url > $file
    local cmd="sfdx force:auth:sfdxurl:store --sfdxurlfile $file --setalias $alias_to_set --json" && (echo $cmd >&2)
    local output=$($cmd)

    sfdx force:config:set defaultusername=$alias_to_set
    sfdx force:config:set defaultdevhubusername=$alias_to_set

    rm $file
  }

  function install_jq() {
    apt update && apt -y install jq
  }

   # Function to install LWC Jest dependencies.
  # Will create or update the package.json with { "test:lwc" : "lwc-jest" } to the scripts property.
  # No arguments.

  function install_lwc_jest() {

    # Create a default package.json if file doesn't exist
    if [ ! -f "package.json" ]; then
      npm init -y
    fi

    # Check if the scripts property in package.json contains key for "test:lwc"
    local scriptValue=$(jq -r '.scripts["test:lwc"]' < package.json)

    # If no "test:lwc" script property, then add one
    if [[ -z "$scriptValue" || $scriptValue == null ]]; then
      local tmp=$(mktemp)
      jq '.scripts["test:lwc"]="lwc-jest"' package.json > $tmp
      mv $tmp package.json
      echo "added test:lwc script property to package.json" >&2
      cat package.json >&2
    fi

    # Now that we have package.json to store dependency references to
    # and to run our lwc jest test scripts, run npm installer
    npm install
    npm install @salesforce/sfdx-lwc-jest@latest --save-dev

  }


  # Checks if there are LWC Jest Test files in any of the package directories of sfdx-project.json.
  # This is necessary because npm will throw error if no test classes are found.
  # No arguments.
  # Returns `true` or `false`

  function check_has_jest_tests() {
    local hasJestTests=false
    for pkgDir in $(jq -r '.packageDirectories[].path' < sfdx-project.json)
    do
      if [ -f $pkgDir ]; then
        local fileCnt=$(find $pkgDir -type f -path "**/__tests__/*.test.js" | wc -l);
        if [ $fileCnt -gt 0 ]; then
          hasJestTests=true
        fi
      fi
    done
    echo $hasJestTests
  }


  # Runs `npm run test:lwc` to execute LWC Jest tests.
  # Function takes no arguments.
  # Should be called after `setup_lwc`.
  # Uses `check_has_jest_tests` to know if there are actually any tests to run.
  # If there aren't any jest tests then npm would throw an error and fail the job,
  # so we skip running npm if there are no tests, essentially skipping them to avoid error.

  function test_lwc_jest() {
    local hasJestTests=$(check_has_jest_tests)
    if [ $hasJestTests ]; then
      npm run test:lwc
    else
      echo 'Skipping lwc tests, found no jest tests in any package directories' >&2
    fi
  }
